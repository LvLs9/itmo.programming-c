#include <stdio.h>
#include <MacTypes.h>


int main() {
    int n;
    char a[255], b[255], buf_a[255], buf_b[255], searchable[255], *last_occur;
    scanf("%s\n%s", a, b);

    // Concat first string with N symbols from second
    scanf("%d", &n);
    memcpy(buf_b, b, n);
    buf_b[n + 1] = '\0';
    printf("%s%s\n", a, buf_b);

    // Compare first N symbols from first string with first N symbols from second
    scanf("%d", &n);
    memcpy(buf_a, a, n);
    buf_a[n + 1] = '\0';
    memcpy(buf_b, b, n);
    buf_b[n + 1] = '\0';
    printf("%d\n", strcmp(buf_a, buf_b));

    // Copy first N symbols from one string to another
    scanf("%d", &n);
    memcpy(buf_a, a, n);
    buf_a[n + 1] = '\0';
    strcpy(buf_b, buf_a);
    printf("%s\n", buf_b);

    // Print len of first string's substring containing symbols from second
    scanf("%s", searchable);
    last_occur = strstr(a, searchable);
    if (last_occur == NULL) {
        printf("Can't find string\n");
    } else {
        printf("%d\n", strlen(searchable));
    }

    // Split first string by delimiters from second string
    scanf("%s", searchable);
    last_occur = strtok(a, searchable);
    while (last_occur != NULL) {
        printf("%s\n", last_occur);
        last_occur = strtok(NULL, searchable);
    }

    return 0;
}