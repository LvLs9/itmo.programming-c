#include <stdio.h>
#include <math.h>
#include "square.h"

struct Square get_square_from_console() {
    int x, y;

    printf("Enter X and Y for begin-point: ");
    scanf("%d %d", &x, &y);
    struct Point begin = {x, y};

    printf("Enter X and Y for end-point: ");
    scanf("%d %d", &x, &y);
    struct Point end = {x, y};

    struct Square square = {begin, end};
    return square;
}

int get_x_len(struct Square square) { return abs(square.begin.x - square.end.x); }

int get_y_len(struct Square square) { return abs(square.begin.y - square.end.y); }

double get_area(struct Square square) {
    return get_x_len(square) * get_y_len(square);
}

int get_perimeter(struct Square square) {
    return (get_x_len(square) + get_y_len(square)) * 2;
}
